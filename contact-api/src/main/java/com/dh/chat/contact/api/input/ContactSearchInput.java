package com.dh.chat.contact.api.input;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Santiago Mamani
 */
@Getter
@Setter
public class ContactSearchInput {

    private Boolean withoutAccountOwner;

    private String information;
}
