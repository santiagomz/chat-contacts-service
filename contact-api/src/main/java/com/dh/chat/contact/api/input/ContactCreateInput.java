package com.dh.chat.contact.api.input;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Santiago Mamani
 */
@Getter
@Setter
public class ContactCreateInput {

    private String email;

    private String name;

    private String information;

    private String avatarId;
}
