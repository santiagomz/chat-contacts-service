package com.dh.chat.contact.api.input;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Santiago Mamani
 */
@Getter
@Setter
public class SystemContactCreateInput {

    private Long userId;

    private Long accountId;

    private String email;

    private String name;
}
