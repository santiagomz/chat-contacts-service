package com.dh.chat.contact.service.model.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * @author Santiago Mamani
 */
@StaticMetamodel(Detail.class)
public class Detail_ {
    public static volatile SingularAttribute<Detail, Long> id;

    public static volatile SingularAttribute<Detail, String> information;

    public static volatile SingularAttribute<Detail, Date> createdDate;
}
